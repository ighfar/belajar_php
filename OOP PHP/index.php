
<?php

require('animal.php');
require('ape.php');
require('frog.php');


$object = new Animal("shaun");

echo"Nama Binatang : $object->name <br>"; // "shaun"
echo"Jumlah Kaki : $object->legs <br>"; // 4
echo"Cool Blooded $object->cold_blooded <br><br>"; // "no"


$object3 = new Frog("buduk");
echo"Nama Binatang : $object3->name <br>"; // "shaun"
echo"Jumlah Kaki : $object3->legs <br>"; // 4
echo"Cool Blooded $object3->cold_blooded <br>"; // "no"
echo "Jump : $object3->jump <br><br>";

$object2 = new Ape("kera sakti");
echo"Nama Binatang : $object2->name <br>"; // "shaun"
echo"Jumlah Kaki : $object2->legs <br>"; // 2
echo"Cool Blooded $object2->cold_blooded <br> "; // "no"
echo "Yell : $object2->yell";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>